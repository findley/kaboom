- dioxus is cool, but feels a little bit rough
    - initial dev command in guide didn't work, needed to add `--platform web`
    - usage of signals doesn't exactly match documentation (skill issues on my
    part, but docs show using signal directly, while in reality I need to invoke
    them to get the value inside.) It makes more sense to me to invoke them,
    it's similar to solidjs, but I'm not sure why the docs show usage of them
    without invoking.
    - build times fking suck. They feel particularly bad when working on the
    design.
    - wish the logging was more straightforward for web platform. I just want
    the logs to show in the browser console. I'm not super familiar with this
    SSR/hydration type of system, so maybe I'm being dumb and just don't realize
    where the code is even running.
    - getting the current time on web seems more difficult than it should be.
    Still need to figure it out. In contrast getting random numbers/random
    sampling was no problem, which I would have expected to be more difficult.
    - rsx is dope, but the LSP/tooling for it is a bit sketchy. Don't get proper
      completion most of the time inside of rsx macros.
    - doesn't seem possible to add classes or other attrs to the document
    element (it's hidden away). I think this is partially because dioxus is
    xplat, but it would be nice if there was just a base index.html template that I could
    modify how I please.
- it was surprisingly easy to implement minesweeper/kaboom game state and logic
completely independent of the framework, then use it in dioxus components. I
feel like if I tried this with react it would have been a PITA to keep react out
of the app logic.
why, maybe this is the difference between signals and whatever tf react does.
But it always felt difficult to separate the application logic 
- rust iterators are cool - AdjIter
    - but the borrow checker is not cool, I can't write the code how I want.
    - I would have liked for the Game to have and `adj(idx)` method which
    returned an iterator that yielded the adjacent squares like `(adj_idx, square)`
    While it's possible to write this method, actually using it is difficult,
    because I end up with double borrow issues. Instead I have to write it in a
    way that feels a bit less elegant.
- kaboom was more difficult to implement than I expected
- it's nice being able to pull in a rust LP lib and have it compile to wasm
without any issues.
- once it's further along I need to do some performance testing
- testing system/DSL, love it!
    - also tests with cargo are great. builtin, tests directly in file with
    submodule (or in a new test file if necessary).
