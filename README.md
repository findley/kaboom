# Kaboom

[Kaboom](https://pwmarcz.pl/blog/kaboom/) is a variant on Minesweeper. Guessing
is always punished unless there is no safe move, in which case guessing will in
defeat.

## Local Development

This implementation of Kaboom is built with a [Dioxus](https://dioxuslabs.com/),
as Rust UI framework.

```
$ npx tailwindcss -i ./input.css -o ./assets/tailwind.css --watch
```
and
```
dx serve --platform web --port 7171
```
