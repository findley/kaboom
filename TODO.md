# TODO

- [ ] incorporate solver for finding board on outside-click
- [ ] add inputs for size
- [ ] try to pick an initial solution that opens on 0
- [ ] add toggle for cheat debug/cheat mode
- [ ] auto-flag all mines on victory
- [x] try to make solve() more pure
- [x] auto-open 0 label squares (and cleanup open logic)
- [x] improve UI enough to be usable
