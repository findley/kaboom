use crate::components::minesweeper::Board;
use crate::game::minesweeper::*;
use dioxus::prelude::*;

#[component]
pub fn Minesweeper() -> Element {
    let mut ms = use_signal(|| Game::new(DEFAULT_CONFIG));
    rsx! {
        div {
            div { "Screen: {ms().screen:?}" }
            button {
                onclick: move |_| { ms.write().apply(Action::Reset{cfg: DEFAULT_CONFIG})},
                "Reset"
            }
            Board {ms}
        }
    }
}
