use crate::components::kaboom::Board;
use dioxus::prelude::*;

#[component]
pub fn Kaboom() -> Element {
    rsx! {
        div {
            class: "flex items-center flex-col",
            Board {}
        }
    }
}
