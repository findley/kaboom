use dioxus::prelude::*;
use dioxus_logger::tracing::info;

use crate::game::minesweeper::*;
use crate::game::Square;

#[component]
pub fn Board(ms: Signal<Game>) -> Element {
    let board_style = format!(
        r#"
        display: grid;
        grid-template-columns: repeat({}, 2.2rem);
        grid-template-rows: repeat({}, 2.2rem);
    "#,
        ms().cfg.w,
        ms().cfg.h,
    );

    rsx! {
        div {
            style: "{board_style}",
            for (i, s) in ms.read().squares.iter().copied().enumerate() {
                Mine {
                    square: s,
                    on_flag_rotate: move || {
                        ms.write().apply(Action::Flag {idx: i});
                        info!("flag rotate {}", i);
                    },
                    on_open: move || {
                        ms.write().apply(Action::Open {idx: i});
                        info!("open!")
                    },
                }
            }
        }
    }
}

#[component]
fn Mine(square: u8, on_open: EventHandler, on_flag_rotate: EventHandler<()>) -> Element {
    let revealed = Square::revealed(square);
    let mine = Square::mine(square);
    let mine_flag = Square::mine_flag(square);
    let unkn_flag = Square::unkn_flag(square);
    let label = Square::label(square).to_string();
    rsx! {
        div {
            class: "aspect-square p-[1px]",
            div {
                class: "w-full h-full",
                onclick: move |_| { on_open(()) },
                oncontextmenu: move |ev| { ev.prevent_default(); on_flag_rotate(()); },
                style: "background-color: darkred;",
                if revealed {
                    if mine { "x" } else { {label} }
                } else {
                    if mine_flag {
                        "."
                    } else if unkn_flag {
                        "?"
                    } else {
                        "_"
                    }
                }
            }
        }
    }
}
