use dioxus::prelude::*;
use dioxus_elements::input_data::MouseButton;
use dioxus_logger::tracing::info;
use gloo_timers::callback::Timeout;

use crate::game::kaboom::*;
use crate::game::{Screen, Square};

#[derive(Debug, Clone, Copy)]
enum Face {
    Smile,
    Shock,
    Relieved,
    Dead,
    Salute,
}

#[component]
pub fn Board() -> Element {
    let cfg = Config {
        w: 8,
        h: 8,
        mines: 10,
        ..DEFAULT_CONFIG
    };
    let mut ms = use_signal(|| Game::new(cfg.clone()));

    let board_style = format!(
        r#"
        display: inline-grid;
        grid-template-columns: repeat({}, 2.2rem);
        grid-template-rows: repeat({}, 2.2rem);
    "#,
        ms().board.w,
        ms().board.h,
    );

    let mut face = use_signal(|| Face::Smile);

    info!("solving for UI");
    let sln = ms().solve().map(|s| s.squares).ok();

    let total_mines = ms().board.mines;
    let flagged_mines = ms().flagged_mines();

    rsx! {
        div { "Screen: {ms().screen:?}" }
        button {
            onclick: move |_| {
                ms.write().apply(Action::Reset{cfg: cfg.clone()});
                face.set(Face::Smile)
            },
            "Reset"
        }
        div {
            class: "flex content-center relative",
            div { class: "card",
                match face().clone() {
                    Face::Smile => rsx!{ h1 { class: "glitch smile", "🙂" } },
                    Face::Shock => rsx!{ h1 { class: "glitch shock", "😮" } },
                    Face::Relieved => rsx!{ h1 { class: "glitch relieved", "😌" } },
                    Face::Dead => rsx!{ h1 { class: "glitch dead", "😵" } },
                    Face::Salute => rsx!{ h1 { class: "glitch salute", "🫡" } },
                }
            }
            div { class: "absolute card",
            div {
                class: "scanlines",
                h1 { "🙂" }
            }
            }
            div {
                class: "absolute bottom-[30px] left-[64px]",
                div { class: "holo" }
            }
        }
        div {
            "Mines: {flagged_mines}/{total_mines}"
        }
        div {
            style: "{board_style}",
            onmouseleave: move |_ev| {
                if ms.read().screen.is_active() {
                    face.set(Face::Smile)
                }
                info!("mouse leave!")
            },
            for (i, s) in ms.read().board.squares.iter().copied().enumerate() {
                Mine {
                    square: s,
                    sln: sln.as_ref().map(|s| s[i]),
                    on_flag_rotate: move || {
                        ms.write().apply(Action::Flag {idx: i});
                        info!("flag rotate {}", i);
                    },
                    on_open: move || {
                        ms.write().apply(Action::Open {idx: i});
                        match ms.read().screen {
                            Screen::Initial => face.set(Face::Smile),
                            Screen::Playing {.. } => face.set(Face::Relieved),
                            Screen::Defeat {..} => face.set(Face::Dead),
                            Screen::Victory {..} => face.set(Face::Salute),
                        };
                        Timeout::new(1_500, move || {
                            // NOTE: This isn't very safe/correct
                            if ms.read().screen.is_active() {
                                face.set(Face::Smile);
                            }
                        }).forget();
                        info!("open!")
                    },
                    on_pre_open: move || {
                        if ms.read().screen.is_active() {
                            face.set(Face::Shock)
                        }
                        info!("pre-open!");
                    },
                }
            }
        }
    }
}

#[component]
fn Mine(
    square: u8,
    sln: Option<SquareSolution>,
    on_open: EventHandler,
    on_flag_rotate: EventHandler<()>,
    on_pre_open: EventHandler<()>,
) -> Element {
    let revealed = Square::revealed(square);
    let mine = Square::mine(square);
    let mine_flag = Square::mine_flag(square);
    let unkn_flag = Square::unkn_flag(square);
    let label = Square::label(square).to_string();
    rsx! {
        div {
            class: "aspect-square p-[1px]",
            div {
                class: "w-full h-full flex justify-center pt-1 cursor-default",
                class: if !revealed { "opacity-50" },
                onclick: move |_| { on_open(()) },
                onmousedown: move |ev| {
                    if ev.trigger_button() == Some(MouseButton::Primary) {
                        on_pre_open(())
                    }
                    info!("{:?}", ev);
                },
                oncontextmenu: move |ev| {
                    ev.prevent_default(); on_flag_rotate(());
                    info!("{:?}", ev);
                },
                style: "background-color: darkred;",
                if revealed {
                    if mine { "x" } else { {label} }
                } else {
                    // if mine { "x" } else { {label} }
                    if mine_flag {
                        "."
                    } else if unkn_flag {
                        "?"
                    } else {
                        match sln {
                            Some(SquareSolution::Inside) => "i",
                            Some(SquareSolution::Outside) => " ",
                            Some(SquareSolution::Mine) => "m",
                            Some(SquareSolution::MaybeMine) => "?",
                            Some(SquareSolution::Safe) => "s",
                            None => " ",
                        }
                    }
                }
            }
        }
    }
}
