use crate::Route;
use dioxus::prelude::*;

#[component]
pub fn Navbar() -> Element {
    rsx! {
        div {
            id: "navbar",
            Link {
                to: Route::Kaboom {},
                "Kaboom"
            }
            Link {
                to: Route::Minesweeper {},
                "Minesweeper"
            }
            Link {
                to: Route::About {},
                "About"
            }
        }

        Outlet::<Route> {}
    }
}
