mod adj_iter;
pub mod minesweeper;
pub mod kaboom;

use chrono::{DateTime, Utc, TimeDelta};

// FIXME: There's no reason for this to be an enum, it can just be a mod with some consts
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Square {
    Empty = 0,
    Revealed = 0b10000000,
    Mine = 0b01000000,
    FlagMine = 0b00100000,
    FlagUnknown = 0b00010000,
    FlagMask = 0b00110000,
    LabelMask = 0b00001111,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Screen {
    Initial,
    Playing { started_at: DateTime<Utc> },
    Victory { time_taken: TimeDelta },
    Defeat { time_taken: TimeDelta },
}

impl Square {
    pub fn mine(v: u8) -> bool {
        v & Self::Mine as u8 > 0
    }

    pub fn revealed(v: u8) -> bool {
        v & Self::Revealed as u8 > 0
    }

    pub fn mine_flag(v: u8) -> bool {
        v & Self::FlagMine as u8 > 0
    }

    pub fn unkn_flag(v: u8) -> bool {
        v & Self::FlagUnknown as u8 > 0
    }

    pub fn label(v: u8) -> u8 {
        v & Square::LabelMask as u8
    }
}

impl Screen {
    pub fn is_active(&self) -> bool {
        match self {
            Self::Initial | Self::Playing { .. } => true,
            _ => false,
        }
    }
}
