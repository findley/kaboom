pub struct AdjIter {
    idx: usize,
    w: i32,
    h: i32,
    curr: usize,
}

impl AdjIter {
    pub fn new(idx: usize, w: usize, h: usize) -> Self {
        Self {
            idx,
            w: w as i32,
            h: h as i32,
            curr: 0,
        }
    }
}

impl Iterator for AdjIter {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let offsets = [
            -self.w + 1,
            -self.w,
            -self.w - 1,
            -1,
            1,
            self.w - 1,
            self.w,
            self.w + 1,
        ];

        let cx = (self.idx as i32) % self.w;
        while self.curr < offsets.len() {
            let i = self.idx as i32 + offsets[self.curr];
            self.curr += 1;

            let x = i % self.w;
            let no_wrap = x == cx + 1 || x == cx - 1 || x == cx;
            if i >= 0 && i < (self.w * self.h) as i32 && no_wrap {
                return Some(i as usize);
            }
        }

        None
    }
}
