use dioxus_logger::tracing::info;
use rand::seq::SliceRandom;
use rand::{rngs::StdRng, SeedableRng};
use chrono::Utc;

use super::adj_iter::AdjIter;
use super::{Screen, Square};

#[derive(Debug, Clone)]
pub struct Game {
    pub squares: Vec<u8>,
    pub screen: Screen,
    pub cfg: Config,
    rng: StdRng,
}

#[derive(Debug, Clone)]
pub struct Config {
    pub w: usize,
    pub h: usize,
    pub mines: usize,
    pub seed: u64,
}

pub enum Action {
    Flag { idx: usize },
    Open { idx: usize },
    Undo,
    Reset { cfg: Config },
}

pub const DEFAULT_CONFIG: Config = Config {
    w: 8,
    h: 8,
    mines: 10,
    seed: 778,
};

impl Game {
    pub fn new(config: Config) -> Self {
        let cfg = config.validate();
        let board = vec![Square::Empty as u8; cfg.w * cfg.h];
        let rng = rand::rngs::StdRng::seed_from_u64(cfg.seed);

        Self {
            squares: board,
            screen: Screen::Initial,
            cfg,
            rng,
        }
    }

    pub fn is_victory(&self) -> bool {
        // victory when no safe squares exist
        !self.squares.iter().copied().any(|sq| {
            let safe = !Square::mine(sq) && !Square::revealed(sq);
            safe
        })
    }

    pub fn apply(&mut self, action: Action) {
        match action {
            Action::Flag { idx } => {
                if !self.screen.is_active() {
                    return;
                }

                let sq = self.squares[idx];

                // Rotate the flag state (Clear -> Mine -> Unknown)
                if !Square::revealed(sq) {
                    let clear_flags = !(Square::FlagMask as u8);
                    self.squares[idx] = match (Square::mine_flag(sq), Square::unkn_flag(sq)) {
                        (false, false) => sq & clear_flags | Square::FlagMine as u8,
                        (true, _) => sq & clear_flags | Square::FlagUnknown as u8,
                        (_, true) => sq & clear_flags,
                    };
                }
            }
            Action::Open { idx } => {
                if !self.screen.is_active() {
                    return;
                }

                // Initialize mines on first square click
                if self.screen == Screen::Initial {
                    self.set_mines(idx);
                    self.screen = Screen::Playing { started_at: Utc::now() };
                }

                let sq = self.squares[idx];

                let start_time = if let Screen::Playing { started_at } = self.screen { started_at } else { Utc::now() };
                let time_taken = Utc::now() - start_time;
                if !Square::revealed(sq) {
                    if self.open(idx) {
                        self.screen = Screen::Defeat { time_taken };
                    } else if self.is_victory() {
                        self.screen = Screen::Victory { time_taken };
                    }
                } else {
                    let adj_mine_flags = AdjIter::new(idx, self.cfg.w, self.cfg.h)
                        .map(|i| self.squares[i])
                        .filter(|s| *s & Square::FlagMine as u8 > 0)
                        .count();

                    if adj_mine_flags == Square::label(sq) as usize {
                        if self.flood_fill(idx) {
                            self.screen = Screen::Defeat { time_taken };
                        } else if self.is_victory() {
                            self.screen = Screen::Victory { time_taken };
                        }
                    }
                }
            }
            Action::Undo => {
                todo!()
            }
            Action::Reset { cfg } => {
                self.cfg = cfg.validate();
                self.rng = rand::rngs::StdRng::seed_from_u64(self.cfg.seed);
                self.squares = vec![Square::Empty as u8; self.cfg.w * self.cfg.h];
                self.screen = Screen::Initial;
            }
        }
    }

    fn open(&mut self, idx: usize) -> bool {
        let sq = self.squares[idx];

        if Square::mine_flag(sq) || Square::revealed(sq) {
            return false;
        }

        self.squares[idx] |= Square::Revealed as u8;

        if Square::mine(sq) {
            true
        } else if Square::label(sq) == 0 {
            self.flood_fill(idx)
        } else {
            false
        }
    }

    fn flood_fill(&mut self, idx: usize) -> bool {
        for adj_idx in AdjIter::new(idx, self.cfg.w, self.cfg.h) {
            if self.open(adj_idx) {
                return true;
            }
        }
        false
    }

    fn set_mines(&mut self, idx: usize) {
        let len = self.cfg.w * self.cfg.h;
        loop {
            // Reset all squares to 0
            for s in self.squares.iter_mut() {
                *s = 0;
            }

            // Choose random mines
            let mut mines = (0..len).collect::<Vec<usize>>();
            mines.as_mut_slice().shuffle(&mut self.rng);

            let mut collision = false;
            for mi in mines.into_iter().take(self.cfg.mines) {
                if mi == idx {
                    // Try again if first square has a mine
                    info!("Ooops, put a mine on start pos, retrying");
                    collision = true;
                    break;
                }
                info!("Setting mine at {}", mi);
                self.squares[mi] |= Square::Mine as u8;
                for adj_idx in AdjIter::new(mi, self.cfg.w, self.cfg.h) {
                    self.squares[adj_idx] += 1;
                }
            }
            if !collision {
                return;
            }
        }
    }
}

impl Config {
    pub fn validate(self) -> Self {
        let area = self.w * self.h;
        let valid = area >= 2 && self.mines < area && self.mines >= 1;

        if valid {
            self
        } else {
            Self::default()
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        DEFAULT_CONFIG
    }
}
