use anyhow::Result;
use dioxus_logger::tracing::info;
use microlp::{Problem, Variable};
use rand::seq::SliceRandom;
use rand::{rngs::StdRng, SeedableRng};
use chrono::Utc;

use super::adj_iter::AdjIter;
use super::{Screen, Square};

#[derive(Debug, Clone)]
pub struct Game {
    pub board: Board,
    pub screen: Screen,
    minimum_border_mines: usize,
    rng: StdRng,
}

#[derive(Debug, Clone)]
pub struct Board {
    pub squares: Vec<u8>,
    pub w: usize,
    pub h: usize,
    pub mines: usize,
}

#[derive(Debug, Clone)]
pub struct Config {
    pub w: usize,
    pub h: usize,
    pub mines: usize,
    pub seed: u64,
}

pub const DEFAULT_CONFIG: Config = Config {
    w: 8,
    h: 8,
    mines: 10,
    seed: 778,
};

pub enum Action {
    Flag { idx: usize },
    Open { idx: usize },
    Undo,
    Reset { cfg: Config },
}

/// Segment represents a group of squares on the board. All squares belong to
/// one of 4 segments:
///
/// * Inside - Revealed squares which are fully enclosed by other revealed squares. These are the
/// revealed squares with 0 labels. They have no impact on the board's solution other than taking
/// up squares.
/// * BorderInside - Revealed squares which have numbers and are adjacent to mines.
/// * BorderOutside - Unrevealed squares which are adjacent to BorderInside squares. This is where
/// the mines live.
/// * Outside - Unrevealed squares do not yet touch the play border.
#[derive(Debug, Clone, Copy, PartialEq)]
enum Segment {
    Inside,
    BorderInside,  // revealed squares with labels on the border
    BorderOutside, // unrevealed squares on the border
    Outside,
}

#[derive(Debug, Clone)]
pub struct Solution {
    pub squares: Vec<SquareSolution>,
    pub possible_boards: Vec<Vec<u8>>,
    pub minimum_border_mines: usize,
}

/// A vector of Solution is the result of calling solve() on a game.
/// * Inside - Unimportant to the game
/// * Outside - No solution, game rules can decide if this is safe or not
/// * Mine - The square is a mine in all possible solutions
/// * MaybeMine - The square is a mine in some solutions and safe in some solutions
/// * Safe - The square is not a mine in all possible solutions
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SquareSolution {
    Inside,
    Outside,
    Mine,
    MaybeMine,
    Safe,
}

impl Game {
    pub fn new(config: Config) -> Self {
        let cfg = config.validate();
        let squares = vec![Square::Empty as u8; cfg.w * cfg.h];
        let rng = rand::rngs::StdRng::seed_from_u64(cfg.seed);

        Self {
            board: Board {
                squares,
                w: cfg.w,
                h: cfg.h,
                mines: cfg.mines,
            },
            screen: Screen::Initial,
            minimum_border_mines: 0,
            rng,
        }
    }

    pub fn apply(&mut self, action: Action) {
        match action {
            Action::Flag { idx } => {
                if !self.screen.is_active() {
                    return;
                }

                let sq = self.board.squares[idx];

                // Rotate the flag state (Clear -> Mine -> Unknown)
                if !Square::revealed(sq) {
                    let clear_flags = !(Square::FlagMask as u8);
                    self.board.squares[idx] = match (Square::mine_flag(sq), Square::unkn_flag(sq)) {
                        (false, false) => sq & clear_flags | Square::FlagMine as u8,
                        (true, _) => sq & clear_flags | Square::FlagUnknown as u8,
                        (_, true) => sq & clear_flags,
                    };
                }
            }
            Action::Open { idx } => {
                if !self.screen.is_active() {
                    return;
                }
                if self.screen == Screen::Initial {
                    self.screen = Screen::Playing { started_at: Utc::now() };
                }
                info!("opening {idx}");

                let sq = self.board.squares[idx];

                let opens: &[usize] = if !Square::revealed(sq) {
                    info!("{idx} is unrevealed");
                    &[idx]
                } else {
                    let adj_flags = AdjIter::new(idx, self.board.w, self.board.h)
                        .filter(|i| Square::mine_flag(self.board.squares[*i]))
                        .count();
                    if adj_flags == Square::label(self.board.squares[idx]) as usize {
                        info!("{idx} is already revealed, collecting adj squares");
                        &AdjIter::new(idx, self.board.w, self.board.h)
                            .filter(|i| {
                                !Square::revealed(self.board.squares[*i])
                                    && !Square::mine_flag(self.board.squares[*i])
                            })
                            .collect::<Vec<usize>>()
                    } else {
                        info!("{idx} is already revealed, incorrect # of flags, noop");
                        &[]
                    }
                };
                if opens.is_empty() {
                    return;
                }

                info!("expanded opens = {opens:?}");
                let sln = self.solve().unwrap();

                let has_safe = sln.squares.contains(&SquareSolution::Safe);
                let has_uncertain = sln.squares.contains(&SquareSolution::MaybeMine);
                let opened_mine = opens.iter().any(|i| sln.squares[*i] == SquareSolution::Mine);
                let opened_uncertain = opens.iter().any(|i| sln.squares[*i] == SquareSolution::MaybeMine);
                let opened_outside = opens.iter().any(|i| sln.squares[*i] == SquareSolution::Outside);
                let proven_outside_mines = sln.squares.iter().filter(|s| **s == SquareSolution::Mine).count();
                let bad_outside_open = opened_outside && (has_safe || has_uncertain) && proven_outside_mines != self.board.mines;
                let defeat = opened_mine || (opened_uncertain && has_safe) || bad_outside_open;

                info!("has_safe={has_safe}, opened_mine={opened_mine}, opened_uncertain={opened_uncertain}, defeat={defeat}");
                if defeat {
                    if bad_outside_open {
                        info!("bad outside open");
                        self.board.squares = sln
                            .boom_board(opens)
                            .or_else(|| sln.safe_board(opens))
                            .unwrap();
                        fill_outside_mines(&mut self.board, &mut self.rng, opens, true);
                    } else {
                        self.board.squares = sln.boom_board(opens).unwrap();
                        fill_outside_mines(&mut self.board, &mut self.rng, opens, false);
                    }
                    if let Screen::Playing { started_at } = self.screen {
                        self.screen = Screen::Defeat { time_taken: Utc::now() - started_at };
                    }
                } else {
                    info!("squares = {:?}", self.board.squares);
                    self.board.squares = sln.safe_board(opens).unwrap();
                    info!("next squares = {:?}", self.board.squares);
                    fill_outside_mines(&mut self.board, &mut self.rng, opens, false);
                }

                for open in opens.iter().copied() {
                    self.board.squares[open] |= Square::Revealed as u8;
                }
                self.flood_reveal();
                self.minimum_border_mines = sln.minimum_border_mines;

                if self.is_victory() {
                    if let Screen::Playing { started_at } = self.screen {
                        self.screen = Screen::Victory { time_taken: Utc::now() - started_at };
                    }
                }
            }
            Action::Undo => {
                todo!()
            }
            Action::Reset { cfg } => {
                let cfg_validated = cfg.validate();
                // self.rng = rand::rngs::StdRng::seed_from_u64(cfg_validated.seed);
                self.board.squares = vec![Square::Empty as u8; cfg_validated.w * cfg_validated.h];
                self.screen = Screen::Initial;
                self.minimum_border_mines = 0;
            }
        }
    }

    pub fn flagged_mines(&self) -> usize {
        self.board
            .squares
            .iter()
            .filter(|sq| Square::mine_flag(**sq))
            .count()
    }

    pub fn solve(&self) -> Result<Solution> {
        let t0 = Utc::now();
        let segmented_board = segment(&self.board);

        let mut prob = Problem::new(microlp::OptimizationDirection::Minimize);
        let mut vars = vec![None; segmented_board.len()];

        // Create border square variables
        for (idx, seg) in segmented_board.iter().copied().enumerate() {
            match seg {
                Segment::BorderOutside => {
                    vars[idx] = Some(prob.add_binary_var(1.0));
                }
                _ => {}
            }
        }

        // Mine count constraints
        let all_square_vars: Vec<(Variable, f64)> = vars
            .iter()
            .filter_map(|v| v.map(|var| (var, 1.0)))
            .collect();
        info!("There are {} square vars", all_square_vars.len());

        info!(
            "Adding constraint for sum(mines) >= {}",
            self.minimum_border_mines
        );
        prob.add_constraint(
            all_square_vars.as_slice(),
            microlp::ComparisonOp::Ge,
            self.minimum_border_mines as f64,
        );

        info!("Adding constraint for sum(mines) <= {}", self.board.mines);
        prob.add_constraint(
            all_square_vars.as_slice(),
            microlp::ComparisonOp::Le,
            self.board.mines as f64,
        );

        // Adjacent label constraints
        for (idx, seg) in segmented_board.iter().copied().enumerate() {
            match seg {
                Segment::BorderInside => {
                    let adj_vars: Vec<(Variable, f64)> =
                        AdjIter::new(idx, self.board.w, self.board.h)
                            .filter_map(|adj_idx| vars[adj_idx].map(|v| (v, 1.0)))
                            .collect();
                    prob.add_constraint(
                        adj_vars,
                        microlp::ComparisonOp::Eq,
                        Square::label(self.board.squares[idx]) as f64,
                    );
                }
                _ => {}
            }
        }

        info!("Solving");
        let mut base_sln = prob.solve()?;

        let mut possible_boards: Vec<Vec<u8>> = Vec::new();
        possible_boards.push(self.realize_solution(&vars, &base_sln));

        let mut square_solutions = vec![SquareSolution::Inside; vars.len()];
        for (idx, var) in vars.iter().enumerate() {
            square_solutions[idx] = if let Some(v) = var {
                let val = base_sln[*v];
                if val > 0.0 {
                    // TODO: Check for answer in possible_boards first
                    if let Ok(sln) = base_sln.fix_var(*v, 0.0) {
                        possible_boards.push(self.realize_solution(&vars, &sln));
                        base_sln = sln.unfix_var(*v).0;
                        SquareSolution::MaybeMine
                    } else {
                        base_sln = prob.solve().unwrap();
                        SquareSolution::Mine
                    }
                } else {
                    // TODO: Check for answer in possible_boards first
                    if let Ok(sln) = base_sln.fix_var(*v, 1.0) {
                        possible_boards.push(self.realize_solution(&vars, &sln));
                        base_sln = sln.unfix_var(*v).0;
                        SquareSolution::MaybeMine
                    } else {
                        base_sln = prob.solve().unwrap();
                        SquareSolution::Safe
                    }
                }
            } else {
                match segmented_board[idx] {
                    Segment::Inside | Segment::BorderInside => SquareSolution::Inside,
                    Segment::BorderOutside => unreachable!(
                        "There should be a var for each square in the BoarderOutside segment"
                    ),
                    Segment::Outside => SquareSolution::Outside,
                }
            }
        }

        let minimum_border_mines = square_solutions
            .iter()
            .filter(|&&s| s == SquareSolution::Mine)
            .count();

        let duration = Utc::now() - t0;
        info!("solve() produced {} possible boards in {}ms", possible_boards.len(), duration.num_milliseconds());

        Ok(Solution {
            squares: square_solutions,
            possible_boards,
            minimum_border_mines,
        })
    }

    fn flood_reveal(&mut self) {
        let mut stack = Vec::new();
        for (idx, &sq) in self.board.squares.iter().enumerate() {
            if Square::revealed(sq) && Square::label(sq) == 0 {
                stack.push(idx);
            }
        }

        while let Some(idx) = stack.pop() {
            for adj_idx in AdjIter::new(idx, self.board.w, self.board.h) {
                let sq = self.board.squares[adj_idx];
                if !Square::revealed(sq) {
                    self.board.squares[adj_idx] |= Square::Revealed as u8;
                    if Square::label(sq) == 0 {
                        stack.push(adj_idx);
                    }
                }
            }
        }
    }

    fn realize_solution(&self, vars: &Vec<Option<Variable>>, sln: &microlp::Solution) -> Vec<u8> {
        self.board
            .squares
            .iter()
            .copied()
            .enumerate()
            .map(|(i, sq)| match vars[i] {
                Some(v) if sln[v] > 0.0 => sq | Square::Mine as u8,
                _ => sq & !(Square::Mine as u8),
            })
            .map(|sq| sq & !(Square::LabelMask as u8))
            .collect()
    }

    pub fn is_victory(&self) -> bool {
        // victory when no safe squares exist
        !self.board.squares.iter().copied().any(|sq| {
            let safe = !Square::mine(sq) && !Square::revealed(sq);
            safe
        })
    }
}

impl Solution {
    fn safe_board(&self, opens: &[usize]) -> Option<Vec<u8>> {
        let safe_board = self
            .possible_boards
            .iter()
            .find(|b| opens.iter().all(|open| !Square::mine(b[*open])))
            .cloned();
        safe_board
    }

    fn boom_board(&self, opens: &[usize]) -> Option<Vec<u8>> {
        let boom_board = self
            .possible_boards
            .iter()
            .find(|b| opens.iter().any(|open| Square::mine(b[*open])))
            .cloned();
        boom_board
    }
}

fn segment(board: &Board) -> Vec<Segment> {
    let mut indexes: Vec<Segment> = vec![Segment::Outside; board.w * board.h];
    for (idx, sq) in board.squares.iter().copied().enumerate() {
        if !Square::revealed(sq) {
            let neighbor_revealed = AdjIter::new(idx, board.w, board.h)
                .any(|adj_idx| Square::revealed(board.squares[adj_idx]));
            if neighbor_revealed {
                indexes[idx] = Segment::BorderOutside;
            } else {
                indexes[idx] = Segment::Outside;
            }
        } else {
            let neighbor_unrevealed = AdjIter::new(idx, board.w, board.h)
                .any(|adj_idx| !Square::revealed(board.squares[adj_idx]));
            if neighbor_unrevealed {
                indexes[idx] = Segment::BorderInside;
            } else {
                indexes[idx] = Segment::Inside;
            }
        }
    }
    indexes
}

fn fill_outside_mines(board: &mut Board, rng: &mut StdRng, opens: &[usize], force_open_mine: bool) {
    let border_mines = board
        .squares
        .iter()
        .filter(|sq| *sq & Square::Mine as u8 > 0)
        .count();
    let outside_mines = board.mines - border_mines;

    // hold back one mine to place under opens[0]
    let outside_mines_to_place = if force_open_mine && outside_mines > 0 {
        outside_mines - 1
    } else {
        outside_mines
    };

    let mut outside_squares: Vec<usize> = segment(&board)
        .iter()
        .enumerate()
        .filter_map(|(i, seg)| match seg {
            Segment::Outside if !opens.contains(&i) => Some(i),
            _ => None,
        })
        .collect();
    outside_squares.shuffle(rng);

    // Set outside mines
    for idx in outside_squares.into_iter().take(outside_mines_to_place) {
        board.squares[idx] |= Square::Mine as u8;
    }
    if force_open_mine && outside_mines > 0 {
        board.squares[opens[0]] |= Square::Mine as u8;
    }

    // Update labels
    for idx in 0..board.squares.len() {
        if Square::mine(board.squares[idx]) {
            for adj_idx in AdjIter::new(idx, board.w, board.h) {
                board.squares[adj_idx] += 1;
            }
        }
    }
}

impl Config {
    pub fn validate(self) -> Self {
        let area = self.w * self.h;
        let valid = area >= 2 && self.mines < area && self.mines >= 1;

        if valid {
            self
        } else {
            Self::default()
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        DEFAULT_CONFIG
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    #[rustfmt::skip]
    fn kaboom_blog_sample() {
        check_board("
            2 mines
            M ? 1
            2 ? 1
        ");
    }

    #[test]
    #[rustfmt::skip]
    fn all_edge_deadly() {
        check_board("
            3 mines
            3 M _
            M M _
            _ _ _
        ");
    }

    #[test]
    #[rustfmt::skip]
    fn max_mines_deduction() {
        check_board("
            3 mines
            2 M 1 0
            M 3 2 1
            1 2 M S
            0 1 S _
        ");
    }

    #[test]
    #[rustfmt::skip]
    fn first_open_safe() {
        check_board("
            5 mines
            _  _  _  _
            _ *_  _  _
            _  _  _  _
            _  _  _  _
        ");
    }

    #[test]
    #[rustfmt::skip]
    fn no_safe_board_on_proven_mine() {
        let (game, _sln_expect, opens) = parse_test_board("
            2 mines
            *M ?  1
            2  ?  1
        ");

        let sln = game.solve().unwrap();
        let safe_board = sln.safe_board(&opens);
        let boom_board = sln.boom_board(&opens);

        assert_eq!(sln.squares[opens[0]], SquareSolution::Mine);
        assert!(safe_board.is_none());
        assert!(Square::mine(boom_board.map(|b| b[opens[0]]).unwrap()));
    }
    #[test]
    #[rustfmt::skip]
    fn no_unsafe_board_on_proven_safe() {
        let (game, _sln_expect, opens) = parse_test_board("
            2 mines
            M  M  1
            2 *S  1
            0  0  0  
        ");

        let sln = game.solve().unwrap();
        let safe_board = sln.safe_board(&opens);
        let boom_board = sln.boom_board(&opens);

        assert_eq!(sln.squares[opens[0]], SquareSolution::Safe);
        assert!(boom_board.is_none());
        assert!(!Square::mine(safe_board.map(|b| b[opens[0]]).unwrap()));
    }

    #[test]
    #[rustfmt::skip]
    fn foo() {
        let (mut game, _sln_expect, _opens) = parse_test_board("
            3 mines
            _ _ _
            _ _ _
            _ _ _
        ");

        let sln = game.solve().unwrap();
        println!("sln: {:?}", sln.squares);
        let sqrs = sln.possible_boards[0].to_owned();
        game.board.squares = sqrs;

        fill_outside_mines(&mut game.board, &mut game.rng, &[0], true);
        println!("boards: {:?}", game.board.squares);
    }

    fn check_board(board: &'static str) {
        let (game, sln_expect, _opens) = parse_test_board(board);

        let sln = game.solve().unwrap();

        println!("solution: {:?}", sln);
        println!("expected: {:?}", sln_expect);
        assert_eq!(sln.squares, sln_expect);
    }

    /// A test board is an ASCII representation of the game board state to make tests easier to
    /// reason about. Rules:
    /// - The first line {num_mines} mines
    /// - Remaining lines constitute the board.
    /// - Use numbers for labels (use 0 for blank squares). The solution is produced using only the
    /// labels.
    /// - Use 'M', '?', 'S', or '_' for any unrevealed squares. This produces the expected
    /// solution.
    ///     - 'M' indicates a proven mine is expected
    ///     - 'S' indicates a proven safe square is expected
    ///     - '?' indicates a maybe-mine is expected
    ///     - '_' indicates an unrevealed square that is outside of the boundary segment
    /// - Prefix a square character with '*' to indicate to indicate the user is opening it.
    /// This can be used to check if correct safe and unsafe board outcomes were generated by
    /// solve()
    fn parse_test_board(str: &'static str) -> (Game, Vec<SquareSolution>, Vec<usize>) {
        let mut squares = Vec::<u8>::new();
        let mut sln_expect = Vec::<SquareSolution>::new();
        let mut mines = 1;
        let mut w = 0;
        let mut h = 0;
        let mut opens: Vec<usize> = vec![];
        for line in str.lines() {
            if line.contains("mines") {
                mines = line
                    .trim()
                    .split_whitespace()
                    .next()
                    .unwrap()
                    .parse::<usize>()
                    .unwrap();
            } else if !line.trim().is_empty() {
                h += 1;
                w = 0;
                for mut s in line.trim().split_whitespace() {
                    w += 1;
                    if s.starts_with("*") {
                        opens.push(squares.len());
                        s = s.strip_prefix("*").unwrap();
                    }
                    match s {
                        "?" => {
                            squares.push(0);
                            sln_expect.push(SquareSolution::MaybeMine);
                        }
                        "M" => {
                            squares.push(0);
                            sln_expect.push(SquareSolution::Mine);
                        }
                        "S" => {
                            squares.push(0);
                            sln_expect.push(SquareSolution::Safe);
                        }
                        "_" => {
                            squares.push(0);
                            sln_expect.push(SquareSolution::Outside);
                        }
                        s => {
                            let label = s.parse::<u8>().unwrap();
                            squares.push(Square::Revealed as u8 + label);
                            sln_expect.push(SquareSolution::Inside);
                        }
                    };
                }
            }
        }

        println!("Parsed test board has w = {w}, h = {h}, mines = {mines}, squares = {squares:?}, expected solution = {sln_expect:?}");
        let mut g = Game::new(Config {
            w,
            h,
            mines,
            ..Default::default()
        });
        g.board.squares = squares;
        (g, sln_expect, opens)
    }
}
